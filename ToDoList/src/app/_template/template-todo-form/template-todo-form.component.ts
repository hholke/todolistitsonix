import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/_interface/todo';

@Component({
  selector: 'app-template-todo-form',
  templateUrl: './template-todo-form.component.html',
  styleUrls: ['./template-todo-form.component.sass']
})
export class TemplateTodoFormComponent implements OnInit {
  public todo: Todo;

  constructor() { 
    this.todo={
      label:undefined,
      status:false
    }
  }

  ngOnInit() {
  }
  createTodo(): void {
    console.log("createTodo called: " + this.todo.label);
  }
}
