import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from 'src/app/_interface/todo';
import { EventPing } from 'src/app/_interface/eventping';

@Component({
  selector: 'app-template-todo',
  templateUrl: './template-todo.component.html',
  styleUrls: ['./template-todo.component.sass']
})
export class TemplateTodoComponent implements OnInit {
  @Input() todo: Todo;
  @Output() ping: EventEmitter<any> = new EventEmitter<any>();

  constructor() { 
    this.todo={
      label:undefined,
      status:false
    }
  }

  writeConsole(): void {
    console.log(this.todo.label);
  }


  ngOnInit() {
  }

  public changeCheck(): void {
    this.todo.status = !this.todo.status;
    const eventObject: EventPing = {
      label: 'check',
      object: this.todo
    };
    this.ping.emit(eventObject);
  }

  public doDelete(): void {
    const eventObject: EventPing = {
      label: 'delete',
      object: this.todo
    };
    this.ping.emit(eventObject);
  } 
}
