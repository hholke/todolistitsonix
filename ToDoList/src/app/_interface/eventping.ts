import { Todo } from '../_interface/todo';
export interface EventPing {
    label: string;                  // action
    object: Todo;               
}
