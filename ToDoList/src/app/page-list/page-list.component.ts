import { Component, OnInit } from '@angular/core';
import { Todo } from '../_interface/todo';
import { EventPing } from '../_interface/eventping';

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.sass']
})
export class PageListComponent implements OnInit {

  public todoShow: boolean;
  public todoShow2: boolean;
  public todoList: Todo[];
  public todoDoneList: Todo[];

  constructor() {
    this.todoShow=true;
    this.todoShow2=true;
    this.todoList = [
      {label: 'Zeile1', status: false},
      {label: 'Zeile2', status: false},
      {label: 'Zeile3', status: false}
    ];
    this.todoDoneList = [
      {label: 'Zeile11', status: true},
      {label: 'Zeile12', status: true}
    ];
  }

  ngOnInit() {
  }
  update(event: EventPing) {
    console.log(event.object.status);
    if('check' === event.label){
      console.log('%C"${event.label}-Event" wurde getriggert. ','color: green;');
      
      if(!event.object.status) {
          this.todoDoneList.splice(this.todoDoneList.indexOf(event.object), 1);
          this.todoList.push(event.object);
          console.log('done');
      } else {
          this.todoList.splice(this.todoList.indexOf(event.object), 1);
          this.todoDoneList.push(event.object);
          console.log('not done');
      }
    }
    if('delete' === event.label){
      console.log('%C"${event.label}-Event" wurde getriggert. ','color: green;');
      
      if(!event.object.status) {
          this.todoList.splice(this.todoList.indexOf(event.object), 1);
          console.log('done');
      } else {
          this.todoDoneList.splice(this.todoDoneList.indexOf(event.object), 1);
          console.log('not done');
      }
    }
  }
}
